// importing modules

let express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let cors = require('cors');
let router = express.Router();
const xl = require('excel4node');



var url = "mongodb://localhost:27017/admin";


let app = express();



const route = require('./routes/route');

// connect to mongoDB

mongoose.connect('mongodb://localhost:27017/admin', { useNewUrlParser: true });

//on connection
mongoose.connection.on('connected', () => {
    console.log('MongoDb Connected on PORT 27017');
});

//on connection error
mongoose.connection.on('error', (err) => {
    console.log(err);
});

const PORT = 3000;

/*******middlewares*******/

//adding middleware - cors
app.use(cors());

//body-parser
app.use(bodyParser.json());

//routing
app.use('/', route);

/********************************/
app.get('/', (request, response) => {
    response.send('some changes');
});

app.listen(PORT, () => {
    console.log('Server has been started at port : ' + PORT);
});


  
  app.listen(3040, function () {
    console.log('Excel app listening on port 3040');
  });

  router.get('http://localhost:3000/api/getData',function(req,res){
      var a =res;
      console.log(res);
  })

