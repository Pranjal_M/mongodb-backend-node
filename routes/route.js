let express = require('express');
let router = express.Router();
const xl = require('excel4node');


let User = require("../model/userInfo");
let regDetails = []

//retreiving data from database
router.get('/getData', (req, res, next) => {
    
    User.find(function(err, data) {
        if (err) {
            res.json(err);
        } else {
            res.json(data);


        }
    });
},err => {

    alert(err);
});


// inserting new data
router.post('/postData', (req, res, next) => {
    // to-do later
    let newUser = new User({
        firstName:req.body.firstName,
        lastName: req.body.lastName,
	    email:req.body.email,
	    phone:req.body.phone,
	    dob:req.body.dob,
	    gender:req.body.gender,
	    address1:req.body.address1,
	    address2:req.body.address2,
	    city:req.body.city,
	    state:req.body.state,
	    qualification:req.body.qualification,
	    course:req.body.course,
	    branch:req.body.branch,
	    universityName:req.body.universityName,
	    collegeName:req.body.collegeName,
	    yearOfPassing:req.body.yearOfPassing,
        percentage:req.body.percentage

    });

    newUser.save((err, data) => {
        if (err) {
            res.json(err);
        } else {
            res.json(data);
            // regDetails.push(res.json(data));
            
            
        }
    });
    // regDetails.push(res.json(data));
});

var json = [{"Vehicle":"BMW","Date":"30, Jul 2013 09:24 AM","Location":"Hauz Khas, Enclave, New Delhi, Delhi, India","Speed":42},{"Vehicle":"Honda CBR","Date":"30, Jul 2013 12:00 AM","Location":"Military Road,  West Bengal 734013,  India","Speed":0},{"Vehicle":"Supra","Date":"30, Jul 2013 07:53 AM","Location":"Sec-45, St. Angel's School, Gurgaon, Haryana, India","Speed":58},{"Vehicle":"Land Cruiser","Date":"30, Jul 2013 09:35 AM","Location":"DLF Phase I, Marble Market, Gurgaon, Haryana, India","Speed":83},{"Vehicle":"Suzuki Swift","Date":"30, Jul 2013 12:02 AM","Location":"Behind Central Bank RO, Ram Krishna Rd by-lane, Siliguri, West Bengal, India","Speed":0},{"Vehicle":"Honda Civic","Date":"30, Jul 2013 12:00 AM","Location":"Behind Central Bank RO, Ram Krishna Rd by-lane, Siliguri, West Bengal, India","Speed":0},{"Vehicle":"Honda Accord","Date":"30, Jul 2013 11:05 AM","Location":"DLF Phase IV, Super Mart 1, Gurgaon, Haryana, India","Speed":71}]
// var json = regDetails;
// console.log(json)

const createSheet = () => {

    return new Promise(resolve => {
  
  // setup workbook and sheet
  var wb = new xl.Workbook();
  
  var ws = wb.addWorksheet('Sheet');
  
  // Add a title row
  
  ws.cell(1, 1)
    .string('Vehicle')
  
  ws.cell(1, 2)
    .string('Date')
  
  ws.cell(1, 3)
    .string('Location')
  
  ws.cell(1, 4)
    .string('Speed')
  
  // add data from json
  
  for (let i = 0; i < json.length; i++) {
  
    let row = i + 2
  
    ws.cell(row, 1)
      .string(json[i].Vehicle)
  
    ws.cell(row, 2)
      .date(json[i].Date)
  
    ws.cell(row, 3)
      .string(json[i].Location)
  
    ws.cell(row, 4)
      .number(json[i].Speed)
  }
  
  resolve( wb )
  
    })
  }
  
  router.get('/excel', function (req, res) {
  
    createSheet().then( file => {
      file.write('ExcelFile.xlsx', res);
    })
  
  });

// updating new Event data
router.put('/putData/:id', (req, res, next) => {
    User.findOneAndUpdate({ _id: req.params.id }, {
            $set: {
                firstName:req.body.firstName,
                lastName: req.body.lastName,
	            email:req.body.email,
	            phone:req.body.phone,
	            dob:req.body.dob,
	            gender:req.body.gender,
	            address1:req.body.address1,
	            address2:req.body.address2,
	            city:req.body.city,
	            state:req.body.state,
	            qualification:req.body.qualification,
	            course:req.body.course,
	            branch:req.body.branch,
	            universityName:req.body.universityName,
	            collegeName:req.body.collegeName,
	            yearOfPassing:req.body.yearOfPassing,
                percentage:req.body.percentage
            }
        },
        function(err, data) {
            if (err) {
                res.json(err);
            } else {
                res.json(data);
            }
        })
});

// deleting new data
router.delete('/deleteData/:id', (req, res, next) => {
    User.remove({ _id: req.params.id }, function(err, data) {
        if (err) {
            res.json(err);
        } else {
            res.json(data);
        }
    })
});



module.exports = router;